import { LeftMenuModel } from '../Model/left-menu-model';
import { SkilCardModel, SkilCardEx } from '../Model/skil-card-model';
import { SkilcardDataProvider } from '../Services/skilcard-data-provider';


export class SkilcardTestdataProviderService implements SkilcardDataProvider {

  public GetAllSkilCard(): Promise<SkilCardModel[]> {

    let SkilCards: SkilCardModel[] = [];

    for (var i = 0; i < 16; i++) {
      SkilCards.push(new SkilCardModel(i,"Программирование на С#",
          "../../assets/images/sharp.jpg",
          (i % 5) + 1, 4, 1000 - (i % 5), 10, 15, 16));
    };

    let result: Promise<SkilCardModel[]> = new Promise((resolve, reject) => {

      setTimeout(function () { resolve(SkilCards) }, 5000);

      // выполняется асинхронная операция, которая в итоге вызовет:
      //
      //   resolve(someValue); // успешное завершение
      // или
      //   reject("failure reason"); // неудача
    });

    return result;
  }
}
