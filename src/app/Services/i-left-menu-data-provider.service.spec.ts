import { TestBed } from '@angular/core/testing';

// @ts-ignore
import { ILeftMenuDataProviderService } from './i-left-menu-data-provider.service';

describe('ILeftMenuDataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ILeftMenuDataProviderService = TestBed.get(ILeftMenuDataProviderService);
    expect(service).toBeTruthy();
  });
});
