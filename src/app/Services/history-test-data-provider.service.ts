import { Injectable } from '@angular/core';
import {AbstractHistoryDataProvider} from './AbstractHistoryDataProvider';
import {SkilRowModel, SkilRowType} from '../Model/skil-card-model';
import {HistoryDayCard} from '../Model/history-day-card';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class HistoryTestDataProviderService implements AbstractHistoryDataProvider{

  constructor() { }

  GetAllHistory(): Promise<HistoryDayCard[]> {

    let History: HistoryDayCard[] = [];
    let ModelForReturn: SkilRowModel[] = [];

    var d = new Date();
    d.setDate(d.getDate()-5);

    var d3 = new Date();
    d3.setDate(d.getDate()-6);

    var d2 = new Date();
    d2.setDate(d2.getDate()-10);

    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "../../assets/images/sharp.jpg", d,5, "Дома", 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "../../assets/images/sharp.jpg", d,5, "Дома" , 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/images/sharp.jpg", d,5 , "Дома", 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "../../assets/images/sharp.jpg", d3,5, "Дома", 15, "Программирование на С+++" ));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "../../assets/images/sharp.jpg", d3,5, "Дома" , 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/images/sharp.jpg", d3,5 , "Дома", 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "../../assets/images/sharp.jpg", d2,5, "Дома", 15, "Программирование на С+++" ));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "../../assets/images/sharp.jpg", d2,5, "Дома" , 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/images/sharp.jpg", d2,5 , "Дома", 15, "Программирование на С+++"));

    let HisoryGrouped = new Map();
    ModelForReturn.forEach(function (value) {

      let row = HisoryGrouped.get(HistoryTestDataProviderService.DateToKey(value.DateStudy))
      if(isNullOrUndefined(row)){
        let rowModel: SkilRowModel[] = [];
        rowModel.push(value);
        let dayCard: HistoryDayCard = HistoryDayCard.GetNewHistoryCard(value.DateStudy,rowModel)
        HisoryGrouped.set(HistoryTestDataProviderService.DateToKey(value.DateStudy), dayCard);
      }
      else{
        (<HistoryDayCard> row).HistoryCollection.push(value)
      }
    });

    HisoryGrouped.forEach( function (value){
      let row = (<HistoryDayCard> value);
      History.push(HistoryDayCard.GetNewHistoryCard(row.DateRecord, row.HistoryCollection))
    } );

    History.sort( function(a, b) {
      if (a.DateRecord.getTime() === b.DateRecord.getTime())
        return 0;

      return  b.DateRecord.getTime() - a.DateRecord.getTime();
    });

    if (HistoryTestDataProviderService.GetDefferenceDay(History[0].DateRecord, new Date()) != 0){
      History.unshift(HistoryDayCard.GetEmptyCard(HistoryTestDataProviderService.GetDefferenceDay(History[0].DateRecord, new Date())))
    }

    let AugmentedStory: HistoryDayCard[] = [];
    let HistoryCount: number = History.length;

    if (HistoryCount > 1){

      for (let i = 0; i < HistoryCount; i++){
        if(i === 0 && History[0].IsDowntimeCard){
          AugmentedStory.push(History[0]);
          continue;
        }
        if(i === HistoryCount - 1){
          AugmentedStory.push(History[i]);
        }
        else{
          let deffDay = HistoryTestDataProviderService.GetDefferenceDay(History[i].DateRecord, History[i + 1].DateRecord);
          if (deffDay > 0){
            AugmentedStory.push(History[i]);
            AugmentedStory.push(HistoryDayCard.GetEmptyCard(deffDay));
          }
          else{
            AugmentedStory.push(History[i]);
          }
        }
      }
    }







    let result: Promise<HistoryDayCard[]> = new Promise((resolve, reject) => {

      setTimeout(function () { resolve(AugmentedStory) }, 1000);

      // выполняется асинхронная операция, которая в итоге вызовет:
      //
      //   resolve(someValue); // успешное завершение
      // или
      //   reject("failure reason"); // неудача
    });

    return result;


  }

  public static DateToKey(date: Date) : string {
    return date.getDay().toString() + date.getMonth().toString() + date.getFullYear().toString();
  };

  public static GetDefferenceDay(date1,date2): number {
    var diff = Math.floor(date1.getTime() - date2.getTime());
    var day = 1000 * 60 * 60 * 24;

    return Math.abs(Math.floor(diff / day));
  };

  GetHistoryFromQuery(DateFrom: Date, DateTo: Date, IdFilterType: string, IdFilterPlace: string, IdFilterSkill: string): Promise<HistoryDayCard[]> {
    return undefined;
  }



}
