import { Injectable } from '@angular/core';
import {AbstractHistoryFilterProviderService} from './abstract-history-filter-provider.service';
import {FilterParams} from '../Model/filter-params';
import {HistoryDayCard} from '../Model/history-day-card';

@Injectable({
  providedIn: 'root'
})
export class HistoryFilterTestdataProviderService implements AbstractHistoryFilterProviderService{

  GetPlaceFilterParams(): Promise<FilterParams[]> {
    let SkillParams: FilterParams[] = [];
    SkillParams.push(new FilterParams("","Все места", "place"));
    SkillParams.push(new FilterParams("дома","дома", "place"));
    SkillParams.push(new FilterParams("на работе","на работе", "place"));
    SkillParams.push(new FilterParams("в дороге","в дороге", "place"));

    let result: Promise<FilterParams[]> = new Promise((resolve, reject) => {
      setTimeout(function () { resolve(SkillParams) }, 1000);
    });

    return result;
  }

  GetSkillFilterParams(): Promise<FilterParams[]> {
    let SkillParams: FilterParams[] = [];
    SkillParams.push(new FilterParams("","Все навыки", "skill"));
    SkillParams.push(new FilterParams("1","Программирование на С++", "skill"));
    SkillParams.push(new FilterParams("2","Архитектура высоконагруженных приложений", "skill"));
    SkillParams.push(new FilterParams("3","Информационные технологии", "skill"));

    let result: Promise<FilterParams[]> = new Promise((resolve, reject) => {
      setTimeout(function () { resolve(SkillParams) }, 1000);
    });

    return result;
  }

  GetTypeFilterParams(): Promise<FilterParams[]> {
    let SkillParams: FilterParams[] = [];
    SkillParams.push(new FilterParams("","Все источники", "type"));
    SkillParams.push(new FilterParams("Видео","Видео", "type"));
    SkillParams.push(new FilterParams("Книга","Книга", "type"));
    SkillParams.push(new FilterParams("Статья","Статья", "type"));

    let result: Promise<FilterParams[]> = new Promise((resolve, reject) => {
      setTimeout(function () { resolve(SkillParams) }, 1000);
    });

    return result;

  }
}
