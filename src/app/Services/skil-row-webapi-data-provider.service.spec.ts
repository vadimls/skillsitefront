import { TestBed } from '@angular/core/testing';

import { SkilRowWebapiDataProviderService } from './skil-row-webapi-data-provider.service';

describe('SkilRowWebapiDataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SkilRowWebapiDataProviderService = TestBed.get(SkilRowWebapiDataProviderService);
    expect(service).toBeTruthy();
  });
});
