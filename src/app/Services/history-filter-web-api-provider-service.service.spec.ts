import { TestBed } from '@angular/core/testing';

import { HistoryFilterWebApiProviderService } from './history-filter-web-api-provider.service';

describe('HistoryFilterWebApiProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HistoryFilterWebApiProviderService = TestBed.get(HistoryFilterWebApiProviderService);
    expect(service).toBeTruthy();
  });
});
