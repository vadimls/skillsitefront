import { SkilcardDataProvider } from '../Services/skilcard-data-provider';
import { LeftMenuModel } from '../Model/left-menu-model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { SkilCardModel, SkilCardEx } from '../Model/skil-card-model';

@Injectable()
export class SkilcardWebapiDataProviderService implements SkilcardDataProvider {

    private http: HttpClient;
    private LOCAL_URL: string = "http://localhost:9090/api/web/v1/skilcard";
    private SERVER_URL: string = "http://a0327329.xsph.ru:80/api/web/v1/skilcard";

  constructor(http: HttpClient) {
    this.http = http;
  }

  public GetAllSkilCard(): Promise<SkilCardModel[]> {

    return this.http.get(this.SERVER_URL).pipe(
        map(data => {
          let SkilCards: SkilCardModel[] = [];
          for (let datas of (data as Array<any>)) {
            SkilCards.push(new SkilCardModel(
                datas['Id'],
                datas['Title'],
                datas['PathToIcon'],
                datas['TheoreticalExperience'],
                datas['PracticalExperience'],
                datas['MoneyCount'],
                datas['VideoCount'],
                datas['BookCount'],
                datas['ArticleCount']));
          }
          return SkilCards;
        })
    ).toPromise();
  }
}
