import { Injectable } from '@angular/core';
import {AbstractHistoryFilterProviderService} from './abstract-history-filter-provider.service';
import {FilterParams} from '../Model/filter-params';
import {HistoryDayCard} from '../Model/history-day-card';
import {HttpClient, HttpParams} from '@angular/common/http';
import {SkilRowModel, SkilRowType} from '../Model/skil-card-model';
import {map} from 'rxjs/operators';
import {DEBUG} from '@angular/compiler-cli/ngcc/src/logging/console_logger';
import {__await} from 'tslib';
import {ThreadSleeper} from '../Tools/threadSleeper';

@Injectable({
  providedIn: 'root'
})
export class HistoryFilterWebApiProviderService implements AbstractHistoryFilterProviderService{

  private http: HttpClient;
  private cachedFilterParams: FilterParams[] = [];
  private downloadCompleate = false;

  private readonly HISTORY_URL: string = "http://a0327329.xsph.ru:80/api/web/v1/history";
  private readonly HISTORY_URL_LOCAL: string = "http://localhost:9090/api/web/v1/history";

  constructor(http: HttpClient) {
    this.http = http;
    this.downloadFilterParams()
  }

  async GetPlaceFilterParams(): Promise<FilterParams[]> {
      while (true){
          if (this.cacheNotEmpty()) {
              return this.GetFilterParamsByType("place");
          }else {
              await ThreadSleeper.Delay(500);
          }
      }
  }

  async GetSkillFilterParams(): Promise<FilterParams[]> {
      while (true){
          if (this.cacheNotEmpty()) {
              return this.GetFilterParamsByType("skill");
          }else {
              await ThreadSleeper.Delay(500);
          }
      }
  }

  async GetTypeFilterParams(): Promise<FilterParams[]> {
      while (true){
          if (this.cacheNotEmpty()) {
             return this.GetFilterParamsByType("type");
          }else {
              await ThreadSleeper.Delay(500);
          }
      }
  }

  private GetFilterParamsByType(type: string): Promise<FilterParams[]>{
      return  new Promise((resolve, reject) => {
          let params = this.cachedFilterParams.filter(param => {
              return param.Type == type
          });
          resolve(params);
      });
  }

  private async downloadFilterParams(): Promise<FilterParams[]> {

      return new Promise((resolve, reject) => {
          let params = new HttpParams().set('queryType', "GetHistoryParams");
          let ModelForReturn: FilterParams[] = [];
          this.http.get(this.HISTORY_URL, { params: params}).pipe(
              map(data => {
                  for (let datas of (data as Array<any>)) {
                      ModelForReturn.push(new FilterParams(
                          datas['Id'],
                          datas['Name'],
                          datas['Type']));
                  }
                  return ModelForReturn;
              })
          ).subscribe((g:FilterParams[]) => {
              this.cachedFilterParams = <FilterParams[]>g;
              this.downloadCompleate = true;
              resolve(g);
          });
      });
  }

  private cacheNotEmpty(): Boolean{
    return this.downloadCompleate;
  }
}

