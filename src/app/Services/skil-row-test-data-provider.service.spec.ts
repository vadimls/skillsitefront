import { TestBed } from '@angular/core/testing';

import { SkilRowTestDataProviderService } from './skil-row-test-data-provider.service';

describe('SkilRowTestDataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SkilRowTestDataProviderService = TestBed.get(SkilRowTestDataProviderService);
    expect(service).toBeTruthy();
  });
});
