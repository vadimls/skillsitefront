import { LeftMenuModel } from '../Model/left-menu-model';
import {SkilCardModel, SkilCardEx, SkilRowModel} from '../Model/skil-card-model';

export abstract class AbstractSkilRowDataProvider {
    abstract GetAllSkilRow(idSkil: number): Promise<SkilRowModel[]>;
}
