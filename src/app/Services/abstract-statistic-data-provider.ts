import {StatisticsModel} from '../Model/statistics-model';
import {StatisticCard} from '../Model/statistic-card';


export abstract class AbstractStatisticDataProvider {
    abstract GetStat(statisticCard: StatisticCard): Promise<StatisticsModel>;
    abstract GetAllStatisticCards(): Promise<StatisticCard[]>;
}
