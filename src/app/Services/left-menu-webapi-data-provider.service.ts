import { ILeftMenuDataProvider } from '../Services/i-left-menu-data-provider.service';
import { LeftMenuModel } from '../Model/left-menu-model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LeftMenuWebapiDataProviderService implements ILeftMenuDataProvider {

  http: HttpClient

  constructor(http: HttpClient) {
    this.http = http;
  }

  GetLeftMenuNodel(): Promise<LeftMenuModel> {

      const httpOptions = {
          headers: new HttpHeaders({
              'Access-Control-Allow-Origin':'*',
              'Access-Control-Allow-Methods':'GET',
              'Access-Control-Allow-Headers': 'Content-Type, Accept'
          })
      };

    return this.http.get('http://a0327329.xsph.ru:80/api/web/v1/leftmenu',httpOptions )
        .pipe(
            map(
                data => {

                  let menuModel = new LeftMenuModel();
                  menuModel.totalPoint = data['TotalPoint'];
                  menuModel.FullName = data['FullName'];
                  menuModel.AddCoin = data['AddCoin'];
                  menuModel.ArticleCount = data['ArticleCount'];
                  menuModel.BookCount = data['BookCount'];
                  menuModel.VideoCount = data['VideoCount'];
                  menuModel.AddArticleCount = data['AddArticleCount'];
                  menuModel.AddBookCount = data['AddBookCount'];
                  menuModel.AddVideoCount = data['AddVideoCount'];

                  return menuModel;
                }
            )
        ).toPromise();
  }
}
