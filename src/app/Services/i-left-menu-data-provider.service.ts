import { LeftMenuModel } from '../Model/left-menu-model';

export abstract class ILeftMenuDataProvider {

  abstract GetLeftMenuNodel(): Promise<LeftMenuModel>;
}
