import { TestBed } from '@angular/core/testing';

import { LeftMenuWebapiDataProviderService } from './left-menu-webapi-data-provider.service';

describe('LeftMenuWebapiDataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LeftMenuWebapiDataProviderService = TestBed.get(LeftMenuWebapiDataProviderService);
    expect(service).toBeTruthy();
  });
});
