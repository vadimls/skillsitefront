import { TestBed } from '@angular/core/testing';

import { SkilcardWebapiDataProviderService } from './skilcard-webapi-data-provider.service';

describe('SkilcardWebapiDataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SkilcardWebapiDataProviderService = TestBed.get(SkilcardWebapiDataProviderService);
    expect(service).toBeTruthy();
  });
});
