import { TestBed } from '@angular/core/testing';

import { SkilcardTestdataProviderService } from './skilcard-testdata-provider.service';

describe('SkilcardTestdataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SkilcardTestdataProviderService = TestBed.get(SkilcardTestdataProviderService);
    expect(service).toBeTruthy();
  });
});
