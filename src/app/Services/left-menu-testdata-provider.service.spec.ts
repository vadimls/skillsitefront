import { TestBed } from '@angular/core/testing';

import { LeftMenuTestdataProviderService } from './left-menu-testdata-provider.service';

describe('LeftMenuTestdataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LeftMenuTestdataProviderService = TestBed.get(LeftMenuTestdataProviderService);
    expect(service).toBeTruthy();
  });
});
