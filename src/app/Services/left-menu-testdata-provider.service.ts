import { ILeftMenuDataProvider } from '../Services/i-left-menu-data-provider.service';
import { LeftMenuModel } from '../Model/left-menu-model';


export class LeftMenuTestdataProviderService implements ILeftMenuDataProvider {

  GetLeftMenuNodel(): Promise<LeftMenuModel> {

    let menuModel = new LeftMenuModel();
    menuModel.totalPoint = 10100;
    menuModel.FullName = "Лысов Вадим";
    menuModel.AddCoin = 1777;

    menuModel.ArticleCount = 1200;
    menuModel.BookCount = 12;
    menuModel.VideoCount = 25;
    menuModel.AddArticleCount = 88;
    menuModel.AddBookCount = 0;
    menuModel.AddVideoCount = 3;

    const result: Promise<LeftMenuModel> = new Promise((resolve, reject) => {

      setTimeout(function () { resolve(menuModel) }, 10000);

      // выполняется асинхронная операция, которая в итоге вызовет:
      //
      //   resolve(someValue); // успешное завершение
      // или
      //   reject("failure reason"); // неудача
    });

    return result;
  }
}
