import { SkilRowModel } from '../Model/skil-card-model';
import {HistoryDayCard} from '../Model/history-day-card';

export abstract class AbstractHistoryDataProvider {
    abstract GetAllHistory(): Promise<HistoryDayCard[]>;
    abstract GetHistoryFromQuery(DateFrom: Date, DateTo: Date, IdFilterType: string, IdFilterPlace: string, IdFilterSkill: string): Promise<HistoryDayCard[]>;
}
