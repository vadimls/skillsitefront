import { TestBed } from '@angular/core/testing';

import { HistoryTestDataProviderService } from './history-test-data-provider.service';

describe('HistoryTestDataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HistoryTestDataProviderService = TestBed.get(HistoryTestDataProviderService);
    expect(service).toBeTruthy();
  });
});
