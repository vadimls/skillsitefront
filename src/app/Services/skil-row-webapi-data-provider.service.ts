import {Injectable} from '@angular/core';
import {AbstractSkilRowDataProvider} from './AbstractSkilRowDataProvider';
import {SkilCardModel, SkilRowModel, SkilRowType} from '../Model/skil-card-model';
import {strictEqual} from 'assert';
import {map} from 'rxjs/operators';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SkilRowWebapiDataProviderService implements AbstractSkilRowDataProvider {

    private http: HttpClient;

    private LOCAL_URL: string = "http://localhost:9090/api/web/v1/skilrow";
    private SERVER_URL: string = "http://a0327329.xsph.ru:80/api/web/v1/skilrow";

  constructor(http: HttpClient) {
    this.http = http;
  }

  GetAllSkilRow(idSkil: number): Promise<SkilRowModel[]> {
      let params = new HttpParams().set('skilid', idSkil.toString());

      return this.http.get(this.SERVER_URL, {params: params}).pipe(
        map(data => {
          let ModelForReturn: SkilRowModel[] = [];
          for (let datas of (data as Array<any>)) {
            ModelForReturn.push(new SkilRowModel(
                datas['Title'],
                SkilRowType[(<string> datas['RowType'])],
                datas['PathToIcon'],
                datas['DateStudy'],
                datas['Id'],
                datas['PlaceOfStudy'],
                datas['MoneyCount'],
                datas['SkillName']));
          }
          return ModelForReturn;
        })
    ).toPromise();
  }
}
