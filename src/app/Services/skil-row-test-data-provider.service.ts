import {Injectable} from '@angular/core';
import {AbstractSkilRowDataProvider} from './AbstractSkilRowDataProvider';
import {SkilCardModel, SkilRowModel, SkilRowType} from '../Model/skil-card-model';
import {strictEqual} from 'assert';

@Injectable({
  providedIn: 'root'
})
export class SkilRowTestDataProviderService implements AbstractSkilRowDataProvider {

  constructor() { }

  GetAllSkilRow(idSkil: number): Promise<SkilRowModel[]> {
    let ModelForReturn: SkilRowModel[] = [];

    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "", new Date(),5, "Дома", 15 , "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "", new Date(),5, "Дома" , 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/SK.png", new Date(),5 , "Дома", 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "", new Date(),5, "Дома", 15 , "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "", new Date(),5 , "Дома", 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/SK.png", new Date(),5, "Дома" , 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "", new Date(),5, "Дома" , 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "", new Date(),5, "Дома" , 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/SK.png", new Date(),5, "Дома", 15 , "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "", new Date(),5, "Дома" , 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "", new Date(),5, "Дома" , 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/SK.png", new Date(),5, "Дома", 15, "Программирование на С+++" ));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "", new Date(),5, "Дома", 15, "Программирование на С+++" ));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "", new Date(),5 , "Дома", 15, "Программирование на С+++"));
    ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/SK.png", new Date(),5, "Дома", 15, "Программирование на С+++" ));

    let result: Promise<SkilRowModel[]> = new Promise((resolve, reject) => {

      setTimeout(function () { resolve(ModelForReturn) }, 3000);

      // выполняется асинхронная операция, которая в итоге вызовет:
      //
      //   resolve(someValue); // успешное завершение
      // или
      //   reject("failure reason"); // неудача
    });

    return result;

  }
}
