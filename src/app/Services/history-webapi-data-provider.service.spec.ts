import { TestBed } from '@angular/core/testing';

import { HistoryWebapiDataProviderService } from './history-webapi-data-provider.service';

describe('HistoryWebapiDataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HistoryWebapiDataProviderService = TestBed.get(HistoryWebapiDataProviderService);
    expect(service).toBeTruthy();
  });
});
