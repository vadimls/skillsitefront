import { TestBed } from '@angular/core/testing';

import { HistoryFilterTestdataProviderService } from './history-filter-testdata-provider.service';

describe('HistoryFilterTestdataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HistoryFilterTestdataProviderService = TestBed.get(HistoryFilterTestdataProviderService);
    expect(service).toBeTruthy();
  });
});
