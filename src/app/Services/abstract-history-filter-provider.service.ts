import { Injectable } from '@angular/core';
import {FilterParams} from '../Model/filter-params';

@Injectable({
  providedIn: 'root'
})
export abstract class AbstractHistoryFilterProviderService {
  abstract GetSkillFilterParams(): Promise<FilterParams[]>;
  abstract GetTypeFilterParams(): Promise<FilterParams[]>;
  abstract GetPlaceFilterParams(): Promise<FilterParams[]>;
}
