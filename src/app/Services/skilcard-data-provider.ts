import { LeftMenuModel } from '../Model/left-menu-model';
import { SkilCardModel, SkilCardEx } from '../Model/skil-card-model';

export abstract class SkilcardDataProvider {
    abstract GetAllSkilCard(): Promise<SkilCardModel[]>;
}
