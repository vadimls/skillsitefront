import {AbstractStatisticDataProvider} from './abstract-statistic-data-provider';
import {StatisticsModel} from '../Model/statistics-model';
import {StatisticDayModel} from '../Model/statistic-day-model';
import {TotalPointModel} from '../Model/total-point-model';
import {StatisticCard, statisticCardType} from '../Model/statistic-card';
import {SkilRowModel, SkilRowType} from '../Model/skil-card-model';
import {isNullOrUndefined} from 'util';
import {map} from 'rxjs/operators';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class StatisticTestDataProvider implements AbstractStatisticDataProvider{

    private monthNames: string[] = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"];
    private static testValue: number = 30;
    private http: HttpClient;
    private allRows: SkilRowModel[] = [];
    private LOCAL_URL: string = "http://localhost:9090/api/web/v1/history";
    private SERVER_URL: string = "http://a0327329.xsph.ru:80/api/web/v1/history";

    constructor(http: HttpClient) {
        this.http = http;
    }

    GetStat(statisticCard: StatisticCard): Promise<StatisticsModel> {

        switch (statisticCard.StatisticCardType) {
            case statisticCardType.Full:{
                break;
            }
            case statisticCardType.Month:{
                return new Promise<StatisticsModel>( (resolve, reject) => { resolve(this.GetStatisticsModelByMonth(statisticCard))});
            }
            case statisticCardType.Year:{
                return new Promise<StatisticsModel>( (resolve, reject) => { resolve(this.GetStatisticsModelByYear(statisticCard))});
            }
        }



        let modelForDaysChart: StatisticsModel = new StatisticsModel();
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('1', 2000, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('2', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('3', 690, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('4', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('5', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('6', 690, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('7', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('8', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('9', 690, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('10', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('11', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('12', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('13', 690, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('14', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('15', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('16', 690, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('17', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('18', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('19', 690, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('21', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('22', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('23', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('24', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('25', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('26', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('27', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('28', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('29', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('30', 90, 100, 140));
        modelForDaysChart.StatisticByDay.push(new StatisticDayModel('31', 90, 100, 140));
        modelForDaysChart.StatisticByPoint.push(new TotalPointModel('Программирование на С#', 90, ""));
        modelForDaysChart.StatisticByPoint.push(new TotalPointModel('Универсальные шаблоны программирования', 100, ""));
        modelForDaysChart.StatisticByType = [[StatisticTestDataProvider.testValue++,60,90]];


        let result: Promise<StatisticsModel> = new Promise((resolve, reject) => {

            setTimeout(function () { resolve(modelForDaysChart) }, 3000);

            // выполняется асинхронная операция, которая в итоге вызовет:
            //
            //   resolve(someValue); // успешное завершение
            // или
            //   reject("failure reason"); // неудача
        });

        return result;
    }

    public GetAllStatisticCards(): Promise<StatisticCard[]>{
        let months: StatisticCard[] = [];

        let params = new HttpParams().set('unlimitedResult', "true");

        let result2: Promise<StatisticCard[]> = new Promise((resolve, reject) => {

            let ModelForReturn: SkilRowModel[] = [];
            this.http.get(this.SERVER_URL, {params: params}).pipe(
                map(data => {
                    for (let datas of (data as Array<any>)) {
                        ModelForReturn.push(new SkilRowModel(
                            datas['Title'],
                            SkilRowType[(<string> datas['RowType'])],
                            datas['PathToIcon'],
                            new Date(datas['DateStudy']),
                            datas['Id'],
                            datas['PlaceOfStudy'],
                            datas['MoneyCount'],
                            datas["SkillName"]));
                    }
                    return ModelForReturn;
                })
            ).subscribe((g:SkilRowModel[]) => {
                this.allRows = g;
                let cards: StatisticCard[] = [];

                let HisoryGrouped = new Map();
                g.forEach(row => {
                    let MonthCard: SkilRowModel[] = HisoryGrouped.get(this.GetDateStamp(row.DateStudy));
                    if (MonthCard != null){
                        MonthCard.push(row);
                    } else {
                        let MC: SkilRowModel[] = [row];
                        HisoryGrouped.set(this.GetDateStamp(row.DateStudy), MC);
                    }
                });
                HisoryGrouped.forEach((value, key, map) => {
                    let rows = (<SkilRowModel[]> value);

                    let statisticCard: StatisticCard = new StatisticCard(this.GetDateTitle(rows[0].DateStudy), statisticCardType.Month,
                                                                         0,0,rows[0].DateStudy.getFullYear(), rows[0].DateStudy.getMonth(),
                                                                         0,0,0,0);

                    let countDays = this.GetCountDayFromMonth(statisticCard.Year,statisticCard.Month);
                    let DayGrouped = new Map();
                    rows.forEach(row => {
                        let day = DayGrouped.get(row.DateStudy.getDate());
                        if (day == null){
                            let dayRows: SkilRowModel[] = [row];
                            DayGrouped.set(row.DateStudy.getDate(), dayRows);

                        }
                        else{
                            DayGrouped.get(row.DateStudy.getDate()).push(row);
                        }
                        statisticCard.MoneyCount += row.MoneyCount;
                    });

                    DayGrouped.forEach((value, key, map) => {
                        statisticCard.TrainingConductedDays++;
                    });
                    statisticCard.NoDaysTraining = countDays - statisticCard.TrainingConductedDays;
                    if (statisticCard.TrainingConductedDays != 0){
                        statisticCard.EfficiencyPercentage = Math.round((statisticCard.TrainingConductedDays / countDays) * 100);
                    }
                    cards.push(statisticCard);
                });

                let MonthGrouped = new Map();

                cards.forEach( card => {

                    if (!MonthGrouped.has(card.Year)){
                        MonthGrouped.set(card.Year, StatisticCard.GetNewYearCard(card.Year.toString(),card.Year));
                    }
                    let year = <StatisticCard>MonthGrouped.get(card.Year);
                    year.MoneyCount += card.MoneyCount;
                    year.TrainingConductedDays += card.TrainingConductedDays;
                    year.NoDaysTraining = 365 - year.TrainingConductedDays;

                    if (year.TrainingConductedDays != 0){
                        year.EfficiencyPercentage = Math.round((year.TrainingConductedDays / 365) * 100);
                    }
                });

                var num = 0;

                let currentYear = new Date().getFullYear();
                let currentMonth = new Date().getMonth();

                if(cards.length > 0){
                    while (true){
                            if(cards[0].Month == currentMonth && cards[0].Year == currentYear){
                                break;
                            }
                            else{
                                let monthNumber;
                                let yearNumber;
                                if(cards[0].Month == 11){
                                    monthNumber = 0;
                                    yearNumber = cards[0].Year + 1;
                                }
                                else{
                                    monthNumber = cards[0].Month + 1;
                                    yearNumber = cards[0].Year;
                                }
                                cards.splice(0, 0, StatisticCard.GetNewMonthCard(this.GetDateTitleByMonth(monthNumber, yearNumber),0,yearNumber,monthNumber))
                            }
                    }
                }

                for (num = 0; num <= cards.length; num++) {
                    if((num + 1) < cards.length){
                        if(this.NextDateIsValid(cards[num].Year, cards[num].Month, cards[num + 1].Year, cards[num + 1].Month)){
                            continue;
                        }
                        else{
                            let monthNumber;
                            let yearNumber;
                            if(cards[num].Month == 0){
                                monthNumber = 11;
                                yearNumber = cards[num].Year - 1;
                            }
                            else{
                                monthNumber = cards[num].Month - 1;
                                yearNumber = cards[num].Year;
                            }
                            cards.splice(num + 1, 0, StatisticCard.GetNewMonthCard(this.GetDateTitleByMonth(monthNumber, yearNumber),0,yearNumber,monthNumber))
                        }
                    }
                }

                cards.reverse();

                MonthGrouped.forEach(year => {
                    cards.unshift(year);
                });

                resolve(cards);

                // выполняется асинхронная операция, которая в итоге вызовет:
                //
                //   resolve(someValue); // успешное завершение
                // или
                //   reject("failure reason"); // неудача
            });
        });

        return result2;
    }

    private NextDateIsValid(currentDateYear: number, currentDateMonth: number, NextDateYear: number, NextDateMonth: number): boolean {
        if(currentDateYear == NextDateYear){
            return currentDateMonth == (NextDateMonth + 1);
        }
        else{
            return NextDateMonth == 11;
        }
    }

    private GetStatisticsModelByMonth(statisticCard: StatisticCard): StatisticsModel{
   //     let rows = this.GetTestSkillRow();

        let rows = this.allRows.filter(x => x.DateStudy.getFullYear() == statisticCard.Year && x.DateStudy.getMonth() == statisticCard.Month);
        rows.sort( function(a, b) {
            if (a.DateStudy.getTime() === b.DateStudy.getTime())
                return 0;

            return  b.DateStudy.getTime() - a.DateStudy.getTime();
        });

        let DayGrouped = new Map();
        rows.forEach((row) => {
            let day = DayGrouped.get(row.DateStudy.getDate());
            if(!isNullOrUndefined(day)){
                day.push(row);
            }
            else {
                let rows: SkilRowModel[] = [];
                rows.push(row);
                DayGrouped.set(row.DateStudy.getDate(), rows);
            }
            }
        );

        let Statistic: StatisticsModel = new StatisticsModel();
        DayGrouped.forEach((value, key, map) => {
            let dayCard:StatisticDayModel = new StatisticDayModel((<string>key),0,0,0);
            (<SkilRowModel[]> value).forEach(row => {

                let statByPoint = Statistic.StatisticByPoint.find(x => x.Header == row.SkillName);
                if (isNullOrUndefined(statByPoint)){
                    Statistic.StatisticByPoint.push(new TotalPointModel(row.SkillName,row.MoneyCount, row.PathToIcon));
                }
                else{
                    statByPoint.Value += row.MoneyCount;
                }

                switch (row.PlaceOfStudy) {
                    case "дома": {
                        dayCard.FirstValue += row.MoneyCount;
                        Statistic.StatisticByType[0][0] += row.MoneyCount;
                        break;
                    }
                    case "на работе": {
                        dayCard.SecondValue += row.MoneyCount;
                        Statistic.StatisticByType[0][1] += row.MoneyCount;
                        break;
                    }
                    case "в дороге": {
                        dayCard.ThirdValue += row.MoneyCount;
                        Statistic.StatisticByType[0][2] += row.MoneyCount;
                        break;
                    }
                }
            });
            Statistic.StatisticByDay.push(dayCard);
        });

        for (let i = 0; i <= this.GetCountDayFromMonth(statisticCard.Year,statisticCard.Month); i++){
            let card = Statistic.StatisticByDay.find(x => x.Header == i.toString());
            if (card == null){
                Statistic.StatisticByDay.unshift(new StatisticDayModel(i.toString(),0,0,0));
            }
        }

        Statistic.StatisticByDay.sort( function(a, b) {
            let numberA = Number.parseInt(a.Header);
            let numberB = Number.parseInt(b.Header);
            if (numberA === numberB)
                return 0;

            return numberA - numberB;
        });

        return Statistic;
    }

    private GetStatisticsModelByYear(statisticCard: StatisticCard): StatisticsModel{

        let rows = this.allRows.filter(x => x.DateStudy.getFullYear() == statisticCard.Year);
        rows.sort( function(a, b) {
            if (a.DateStudy.getTime() === b.DateStudy.getTime())
                return 0;

            return  b.DateStudy.getTime() - a.DateStudy.getTime();
        });

        let DayGrouped = new Map();
        rows.forEach((row) => {
                let day = DayGrouped.get(row.DateStudy.getMonth() + 1);
                if(!isNullOrUndefined(day)){
                    day.push(row);
                }
                else {
                    let rows: SkilRowModel[] = [];
                    rows.push(row);
                    DayGrouped.set(row.DateStudy.getMonth() + 1, rows);
                }
            }
        );

        let Statistic: StatisticsModel = new StatisticsModel();
        DayGrouped.forEach((value, key, map) => {
            let dayCard:StatisticDayModel = new StatisticDayModel((<string>key),0,0,0);
            (<SkilRowModel[]> value).forEach(row => {

                let statByPoint = Statistic.StatisticByPoint.find(x => x.Header == row.SkillName);
                if (isNullOrUndefined(statByPoint)){
                    Statistic.StatisticByPoint.push(new TotalPointModel(row.SkillName,row.MoneyCount,row.PathToIcon));
                }
                else{
                    statByPoint.Value += row.MoneyCount;
                }

                switch (row.PlaceOfStudy) {
                    case "дома": {
                        dayCard.FirstValue += row.MoneyCount;
                        Statistic.StatisticByType[0][0] += row.MoneyCount;
                        break;
                    }
                    case "на работе": {
                        dayCard.SecondValue += row.MoneyCount;
                        Statistic.StatisticByType[0][1] += row.MoneyCount;
                        break;
                    }
                    case "в дороге": {
                        dayCard.ThirdValue += row.MoneyCount;
                        Statistic.StatisticByType[0][2] += row.MoneyCount;
                        break;
                    }
                }
            });
            Statistic.StatisticByDay.push(dayCard);
        });

        for (let i = 1; i <= 12; i++){
            let card = Statistic.StatisticByDay.find(x => x.Header == i.toString());
            if (card == null){
                Statistic.StatisticByDay.unshift(new StatisticDayModel(i.toString(),0,0,0));
            }
        }

        Statistic.StatisticByDay.sort( function(a, b) {
            let numberA = Number.parseInt(a.Header);
            let numberB = Number.parseInt(b.Header);
            if (numberA === numberB)
                return 0;

            return numberA - numberB;
        });

        return Statistic;
    }

    private GetCountDayFromMonth(year:number, month:number): number{
        let t = new Date(year,month,15);
        return( t.setDate(32) && t.setDate(0) && t.getDate())
    }

    private GetTestSkillRow(): SkilRowModel[]{
        let ModelForReturn: SkilRowModel[] = [];

        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "", new Date(2019,3,5),5, "дома", 15 , "Программирование на языке С++"));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "", new Date(2019,3,5),5, "на работе" , 15, "Программирование на языке С++"));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/SK.png", new Date(2019,3,5),5 , "в дороге", 15, "Программирование на языке С++"));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "", new Date(2019,3,6),5, "дома", 15, "Программирование на языке С++" ));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "", new Date(2019,3,6),5 , "дома", 15, "Программирование на языке С++"));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/SK.png", new Date(2019,3,6),5, "дома" , 15, "Программирование на языке С++"));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "", new Date(2019,3,8),5, "на работе" , 15, "Программирование на языке С++"));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "", new Date(2019,3,8),5, "дома" , 15, "Программирование на языке С++"));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/SK.png", new Date(2019,3,8),5, "дома", 15, "Программирование на языке С++" ));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "", new Date(2019,3,13),5, "дома" , 15, "Программирование на языке С++"));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "", new Date(2019,3,13),5, "на работе" , 15, "Программирование на языке С++"));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/SK.png", new Date(2019,3,13),5, "дома", 15, "Программирование на языке С++" ));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.ArticleRow, "", new Date(2019,3,13),5, "дома", 15, "Программирование на языке С++" ));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.VideoRow, "", new Date(2019,3,19),5 , "дома", 15, "Программирование на языке С++"));
        ModelForReturn.push(new SkilRowModel("Поиск утечек памяти", SkilRowType.BookRow, "../../assets/SK.png", new Date(2019,3,20),5, "дома", 15, "Программирование на языке С++" ));

        return ModelForReturn;
    }

    private GetDateStamp(date:Date): string {
        return date.getFullYear().toString() + date.getMonth().toString();
    }

    private GetDateTitle(date:Date): string{
        return this.monthNames[date.getMonth()] + " " + date.getFullYear().toString();
    }

    private GetDateTitleByMonth(Month:number, Year:number): string{
        return this.monthNames[Month] + " " + Year;
    }
}



