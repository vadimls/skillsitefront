import { Injectable } from '@angular/core';
import {AbstractHistoryDataProvider} from './AbstractHistoryDataProvider';
import {SkilRowModel, SkilRowType} from '../Model/skil-card-model';
import {HistoryDayCard} from '../Model/history-day-card';
import { isNullOrUndefined } from 'util'
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HistoryWebapiDataProviderService implements AbstractHistoryDataProvider{

  http: HttpClient;

  private readonly HISTORY_URL: string = "http://a0327329.xsph.ru:80/api/web/v1/history";
  private readonly HISTORY_URL_LOCAL: string = "http://localhost:9090/api/web/v1/history";

  constructor(http: HttpClient) {
    this.http = http;
  }

  GetAllHistory(): Promise<HistoryDayCard[]> {
       return this.GetHistoryFromQuery(null,null,"","","");
  }

  GetHistoryFromQuery(DateFrom: Date, DateTo: Date, IdFilterType: string, IdFilterPlace: string, IdFilterSkill: string): Promise<HistoryDayCard[]> {

      let params = new HttpParams().set('queryType', "")
                                   .set('yearFrom', DateFrom != null ? DateFrom.getFullYear().toString() : "")
                                   .set('monthFrom',DateFrom != null ? DateFrom.getMonth().toString() : "")
                                   .set('dayFrom',DateFrom != null ? DateFrom.getDate().toString() : "")
                                   .set('yearTo',DateTo != null ? DateTo.getFullYear().toString() : "")
                                   .set('monthTo',DateTo != null ? DateTo.getMonth().toString() : "")
                                   .set('dayTo',DateTo != null ? DateTo.getDate().toString() : "")
                                   .set('place',IdFilterPlace)
                                   .set('type',IdFilterType)
                                   .set('knowledgeId',IdFilterSkill);

      let result: Promise<HistoryDayCard[]> = new Promise((resolve, reject) => {

          let ModelForReturn: SkilRowModel[] = [];
          this.http.get(this.HISTORY_URL, { params: params}).pipe(
              map(data => {
                  for (let datas of (data as Array<any>)) {
                      ModelForReturn.push(new SkilRowModel(
                          datas['Title'],
                          SkilRowType[(<string> datas['RowType'])],
                          datas['PathToIcon'],
                          new Date(datas['DateStudy']),
                          datas['Id'],
                          datas['PlaceOfStudy'],
                          datas['MoneyCount'],
                          datas['SkillName']));
                  }
                  return ModelForReturn;
              })
          ).subscribe((g:SkilRowModel[]) => {

              let AugmentedStory: HistoryDayCard[] = HistoryWebapiDataProviderService.SkilRowModelToHistoryDayCard(g, ModelForReturn);
              resolve(AugmentedStory);
          });
      });

      return result;
  }

  public static DateToKey(date: Date) : string {
    return date.getDay().toString() + date.getMonth().toString() + date.getFullYear().toString();
  };

  public static GetDefferenceDay(date1,date2): number {
    var diff = Math.floor(date1.getTime() - date2.getTime());
    var day = 1000 * 60 * 60 * 24;

    return Math.abs(Math.floor(diff / day));
  };

  public static SkilRowModelToHistoryDayCard(h : SkilRowModel[], ModelForReturn: SkilRowModel[]) : HistoryDayCard[] {

      if(h.length == 0){
          let AugmentedStory: HistoryDayCard[] = [];
          return AugmentedStory;
      }

      let HisoryGrouped = new Map();
      ModelForReturn.forEach(function (value) {

          let row = HisoryGrouped.get(HistoryWebapiDataProviderService.DateToKey(value.DateStudy));
          if(isNullOrUndefined(row)){
              let rowModel: SkilRowModel[] = [];
              rowModel.push(value);
              let dayCard: HistoryDayCard = HistoryDayCard.GetNewHistoryCard(value.DateStudy,rowModel);
              HisoryGrouped.set(HistoryWebapiDataProviderService.DateToKey(value.DateStudy), dayCard);
          }
          else{
              (<HistoryDayCard> row).HistoryCollection.push(value)
          }
      });
      let History: HistoryDayCard[] = [];
      HisoryGrouped.forEach( function (value){
          let row = (<HistoryDayCard> value);
          History.push(HistoryDayCard.GetNewHistoryCard(row.DateRecord, row.HistoryCollection))
      } );

      History.sort( function(a, b) {
          if (a.DateRecord.getTime() === b.DateRecord.getTime())
              return 0;

          return  b.DateRecord.getTime() - a.DateRecord.getTime();
      });

      if (HistoryWebapiDataProviderService.GetDefferenceDay(History[0].DateRecord, new Date()) < 2){
          History.unshift(HistoryDayCard.GetEmptyCard(HistoryWebapiDataProviderService.GetDefferenceDay(History[0].DateRecord, new Date())))
      }

      let AugmentedStory: HistoryDayCard[] = [];
      let HistoryCount: number = History.length;

      if (HistoryCount > 1){

          for (let i = 0; i < HistoryCount; i++){
              if(i === 0 && History[0].IsDowntimeCard){
                  AugmentedStory.push(History[0]);
                  continue;
              }
              if(i === HistoryCount - 1){
                  AugmentedStory.push(History[i]);
              }
              else{
                  let deffDay = HistoryWebapiDataProviderService.GetDefferenceDay(History[i].DateRecord, History[i + 1].DateRecord);
                  if (deffDay > 1){
                      AugmentedStory.push(History[i]);
                      AugmentedStory.push(HistoryDayCard.GetEmptyCard(deffDay));
                  }
                  else{
                      AugmentedStory.push(History[i]);
                  }
              }
          }
      }

      return AugmentedStory;
  }


}
