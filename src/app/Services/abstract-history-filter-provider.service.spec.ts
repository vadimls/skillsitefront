import { TestBed } from '@angular/core/testing';

import { AbstractHistoryFilterProviderService } from './abstract-history-filter-provider.service';

describe('AbstractHistoryFilterProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AbstractHistoryFilterProviderService = TestBed.get(AbstractHistoryFilterProviderService);
    expect(service).toBeTruthy();
  });
});
