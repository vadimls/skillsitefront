import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LightleftmenuComponent } from './lightleftmenu.component';

describe('LightleftmenuComponent', () => {
  let component: LightleftmenuComponent;
  let fixture: ComponentFixture<LightleftmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LightleftmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LightleftmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
