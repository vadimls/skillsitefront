

export class StatisticCard {
    public Title: string;
    public StatisticCardType: statisticCardType;
    private statisticCardType = statisticCardType;
    public MoneyCount: number = 0;
    public EfficiencyPercentage: number = 0;
    public Year: number = 0;
    public Month: number = 0;

    public TrainingConductedDays: number;
    public NoDaysTraining: number;
    public LongestBreakDays: number;
    public LongestSeriesDays: number;

    public CardIsActive: boolean = false;

    public constructor(Title: string, StatisticCardType: statisticCardType, MoneyCount: number,
                       EfficiencyPercentage: number, Year: number, Month: number,
                       TrainingConductedDays: number, NoDaysTraining: number, LongestBreakDays: number, LongestSeriesDays: number){
        this.Title = Title;
        this.StatisticCardType = StatisticCardType;
        this.MoneyCount = MoneyCount;
        this.EfficiencyPercentage = EfficiencyPercentage;
        this.Month = Month;
        this.Year = Year;
        this.TrainingConductedDays = TrainingConductedDays;
        this.NoDaysTraining = NoDaysTraining;
        this.LongestBreakDays = LongestBreakDays;
        this.LongestSeriesDays = LongestSeriesDays;
    }

    public static GetNewMonthCard(Title: string, MoneyCount: number, Year: number, Month: number,) :StatisticCard {
        return new StatisticCard(Title, statisticCardType.Month,MoneyCount, 0, Year,Month,0,0,0,0);
    }

    public static GetNewYearCard(Title: string, Year: number) :StatisticCard {
        return new StatisticCard(Title, statisticCardType.Year,0, 0, Year,0,0,0,0,0);
    }
}


export enum statisticCardType {
    Month = 1,
    Year = 2,
    Full = 3
}
