export class TotalPointModel{

    public Header: string;
    public Value: number;
    public PathToIcon: string;

    public constructor(Header: string, Value: number,PathToIcon: string ) {
        this.Header = Header;
        this.Value = Value;
        this.PathToIcon = PathToIcon;
    }
}
