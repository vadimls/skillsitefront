export class SkilCardModel {
    Id: number;
    Title: string;
    PathToIcon: string;
    TheoreticalExperience: number;
    PracticalExperience: number;
    VideoCount: number;
    BookCount: number;
    ArticleCount: number;
    MoneyCount: number;
    SkilCardStatus: SkilCardEx;
    SkillCardVisable: boolean = true;
    SkillCardForRemove: boolean = false;
    private skilCardStatus = SkilCardEx;

    constructor(Id: number, Title: string, PathToIcon: string, TheoreticalExperience: number, PracticalExperience: number,
                MoneyCount: number, VideoCount: number, BookCount: number, ArticleCount: number) {
        this.Title = Title;
        this.PathToIcon = PathToIcon;
        this.TheoreticalExperience = TheoreticalExperience;
        this.PracticalExperience = PracticalExperience;
        this.MoneyCount = MoneyCount;
        this.SkilCardStatus = this.setSkilCardStatus(TheoreticalExperience, PracticalExperience);
        this.Id = Id;
        this.VideoCount = VideoCount;
        this.BookCount = BookCount;
        this.ArticleCount = ArticleCount;
    }

    private setSkilCardStatus(TheoreticalExperience: number, PracticalExperience: number): SkilCardEx {

        switch (Math.min(TheoreticalExperience, PracticalExperience)) {

            case (5):
            case (4):
                return SkilCardEx.Excellent;

            case (3):
                return SkilCardEx.Good;

            case (2):
            case (1):
            default:
                return SkilCardEx.Normally;
        }
    }
}

export class SkilRowModel{
    public Title: string;
    public RowType: SkilRowType;
    private rowType = SkilRowType;
    public PathToIcon: string;
    public DateStudy: Date;
    public Id: Number;
    public PlaceOfStudy: String;
    public MoneyCount: number;
    public SkillName: string;

    constructor(Title: string, RowType: SkilRowType, PathToIcon: string, DateStudy: Date,Id: Number, PlaceOfStudy: String, MoneyCount: number, SkillName: string ) {
        this.Title = Title;
        this.RowType = RowType;
        this.PathToIcon = PathToIcon;
        this.DateStudy = DateStudy;
        this.Id = Id;
        this.PlaceOfStudy = PlaceOfStudy;
        this.MoneyCount = MoneyCount;
        this.SkillName = SkillName;
    }
}


export enum SkilCardEx {
    Excellent = 1,
    Good = 2,
    Normally = 3
}

export enum SkilRowType {
    BookRow = 1,
    ArticleRow = 2,
    VideoRow = 3
}
