import {SkilRowModel} from './skil-card-model';
import {StatisticDayModel} from './statistic-day-model';
import {TotalPointModel} from './total-point-model';
import {TotalPointSkill} from './total-point-skill';

export class StatisticsModel{

    public StatisticByDay: StatisticDayModel[] = [];
    public StatisticByPoint: TotalPointModel[] = [];
    public StatisticByTotalPointSkil: TotalPointSkill[] = [];
    public StatisticByType = [[0,0,0]];

    public constructor( ) {

    }

    public ConvertStatisticByDayToArrayModel() : any[] {
        let convertedStatistic: any[] = [];
        this.StatisticByDay.forEach(function (value) {
            convertedStatistic.push([value.Header, value.FirstValue, value.SecondValue, value.ThirdValue]);
        });
        return convertedStatistic;
    }

    public ConvertStatisticByTotalPointModelToArrayModel() : any[] {
        let convertedStatistic: any[] = [];
        this.StatisticByPoint.forEach(function (value) {
            convertedStatistic.push([value.Header, value.Value]);
        });
        return convertedStatistic;
    }
}
