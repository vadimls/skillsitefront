import {SkilRowModel, SkilRowType} from '../Model/skil-card-model';

export class SkilRowGroup {
    public readonly RowCollection: SkilRowModel[];
    public readonly PathToIcon: string;

    private constructor(GroupTitle: string, RowCollection: SkilRowModel[], ){
        this.PathToIcon = GroupTitle;
        this.RowCollection = RowCollection;
    }

    public static GetNewSkilRowGroup(GroupTitle: string):SkilRowGroup{
        let RowCollection: SkilRowModel[] = [];
        return new SkilRowGroup(GroupTitle,  RowCollection);
    }
}
