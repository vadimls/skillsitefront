export class LeftMenuModel {
    public totalPoint: number;
    public  FullName: string;
    public  AddCoin: number;

    public BookCount: number;
    public ArticleCount: number;
    public VideoCount: number;
    public AddBookCount: number;
    public AddArticleCount: number;
    public AddVideoCount: number;

    public static GetEmptyModel(): LeftMenuModel {

        let menuModel = new LeftMenuModel();
        menuModel.totalPoint = 0;
        menuModel.FullName = "Загрузка";
        menuModel.AddCoin = 0;

        menuModel.ArticleCount = 0;
        menuModel.BookCount = 0;
        menuModel.VideoCount = 0;
        menuModel.AddArticleCount = 0;
        menuModel.AddBookCount = 0;
        menuModel.AddVideoCount = 0;

        return menuModel;
    }

}
