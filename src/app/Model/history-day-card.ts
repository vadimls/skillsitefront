import {SkilRowModel, SkilRowType} from '../Model/skil-card-model';
import {SkilRowGroup} from './skilRowGroup';
import {isNullOrUndefined} from "util";

export class HistoryDayCard{

    public readonly DateRecord: Date;
    public readonly HistoryCollection: SkilRowModel[] = [];
    public readonly HistoryGroups: SkilRowGroup[] = [];
    public readonly IsDowntimeCard: boolean;
    public readonly DowntimeOfCountOfDay: number;
    public TotalMoney: number = 0;

    private constructor(DateRecord: Date, HistoryCollection: SkilRowModel[], IsDowntimeCard: boolean, DowntimeOfCountOfDay: number ) {
        this.DateRecord = DateRecord;
        this.HistoryCollection = HistoryCollection;
        this.HistoryGroups = HistoryDayCard.ConvertSkilRowModelsToSkilRowGroup(HistoryCollection);
        this.IsDowntimeCard = IsDowntimeCard;
        this.DowntimeOfCountOfDay = DowntimeOfCountOfDay;
        if (this.HistoryCollection != null){
            this.HistoryCollection.forEach((row) => this.TotalMoney += row.MoneyCount);
        }
    }

    public static GetEmptyCard(DowntimeOfCountOfDay: number): HistoryDayCard {
        return  new HistoryDayCard(null,null,true,DowntimeOfCountOfDay);
    }

    public static GetNewHistoryCard(DateRecord: Date, HistoryCollection: SkilRowModel[]){
        return  new HistoryDayCard(DateRecord,HistoryCollection,false,0);
    }

    private static ConvertSkilRowModelsToSkilRowGroup(HistoryCollection: SkilRowModel[]) : SkilRowGroup[] {
        let SkilGroups: SkilRowGroup[] = [];

        if (HistoryCollection != null){
            HistoryCollection.forEach((hc)=> {
                let rowGroup = SkilGroups.find((x) => x.PathToIcon === hc.PathToIcon);
                if (rowGroup != undefined){
                    rowGroup.RowCollection.push(hc);
                }
                else {
                    let group = SkilRowGroup.GetNewSkilRowGroup(hc.PathToIcon);
                    group.RowCollection.push(hc);
                    SkilGroups.push(group);
                }
            });
        }

        return SkilGroups;
    }
}
