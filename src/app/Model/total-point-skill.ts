export class TotalPointSkill {

    public Title: string;
    public MoneyCount: number;
    public PathToIcon: string;
    public TotalPercent: number;

    public constructor(Title: string, MoneyCount: number, PathToIcon: string, TotalPercent: number) {
        this.Title = Title;
        this.MoneyCount = MoneyCount;
        this.PathToIcon = PathToIcon;
        this.TotalPercent = TotalPercent;
    }

}
