import {SkilRowModel} from './skil-card-model';

export class StatisticDayModel{

    public Header: string;
    public FirstValue: number = 0;
    public SecondValue: number = 0;
    public ThirdValue: number = 0;

    public constructor(Header: string, FirstValue: number, SecondValue: number, ThirdValue: number  ) {
        this.Header = Header;
        this.FirstValue = FirstValue;
        this.SecondValue = SecondValue;
        this.ThirdValue = ThirdValue;
    }


}
