

export class FilterParams {
    public Id: string;
    public Name: string;
    public Type: string;

    public constructor(Id: string, Name: string, Type: string){
        this.Id = Id;
        this.Name = Name;
        this.Type = Type;
    }
}
