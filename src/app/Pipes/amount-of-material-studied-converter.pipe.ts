import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'amountOfMaterialStudiedConverter'
})
export class AmountOfMaterialStudiedConverterPipe implements PipeTransform {

  transform(value: number, args?: any): string {

    if (value < 1000)
      return value.toString()


    let big = Math.floor(value / 1000);
    let m = big * 1000;
    let remainder = Math.floor((value - m) / 100);

    return big + '.' + remainder + 'к';
  }

}
