import { Pipe, PipeTransform } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'skilPointConverter'
})
export class SkilPointConverterPipe implements PipeTransform {

  transform(value: number, args?: any): string {

    if (isNullOrUndefined(value)) {
      return '';
    }

    if (value < 1000) {
      return value.toString();
    }

    const big = Math.floor(value / 1000);
    const remainder = value % 1000;

    let zero = '';
    if (remainder < 100) {
      if (remainder < 10) {
        zero = '00';
      } else {
        zero = '0';
      }
    }

    return big + '.' + zero + remainder;
  }
}
