import { Component, OnInit } from '@angular/core';
import {HistoryDayCard} from '../Model/history-day-card';
import {SkilRowModel, SkilRowType} from '../Model/skil-card-model';
import {AbstractHistoryDataProvider} from '../Services/AbstractHistoryDataProvider';
import {FilterParams} from '../Model/filter-params';
import {AbstractHistoryFilterProviderService} from '../Services/abstract-history-filter-provider.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  private HistoryDataProvider: AbstractHistoryDataProvider;
  private FilterDataProvider: AbstractHistoryFilterProviderService;

  public History: HistoryDayCard[] = [];
  public HistoryCount = 0;

  public SkillParams: FilterParams[] = [];
  public SelectedSkillFilterParam: FilterParams;
  public TypeParams: FilterParams[] = [];
  public SelectedTypeFilterParam: FilterParams;
  public PlaceParams: FilterParams[] = [];
  public SelectedPlaceFilterParam: FilterParams;
  public SelectedPlaceFilter: string;

  public FilterParamsChanged = false;
  public ShowFilterPanel = false;

  constructor(HistoryDataProvider: AbstractHistoryDataProvider, FilterDataProvider: AbstractHistoryFilterProviderService) {
    this.HistoryDataProvider = HistoryDataProvider;
    this.FilterDataProvider = FilterDataProvider;
  }

  ngOnInit() {
      this.SetDateToInput();
      this.HistoryDataProvider.GetAllHistory().then(model => {this.History = model; this.HistoryCount = model.length});
      this.FilterDataProvider.GetSkillFilterParams().then(model => {this.SkillParams = model});
      this.FilterDataProvider.GetTypeFilterParams().then(model => {this.TypeParams = model});
      this.FilterDataProvider.GetPlaceFilterParams().then(model => {this.PlaceParams = model})}

  public FilterParamsChange() : void {
    this.FilterParamsChanged = true;
  }

  public GeneateNewQuery(){
    if (this.FilterParamsChanged == false)
      return;

    let valueFilterType = (<HTMLInputElement>document.getElementById("TypeParamInput")).value;
    let valueFilterPlace = (<HTMLInputElement>document.getElementById("PlaceParamInput")).value;
    let valueFilterSkill = (<HTMLInputElement>document.getElementById("SkillParamInput")).value;
    let dateFrom = new Date((<HTMLInputElement>document.getElementById("dateFrom")).value);
    let dateTo = new Date((<HTMLInputElement>document.getElementById("dateTo")).value);

    this.FilterParamsChanged = false;

    this.HistoryDataProvider.GetHistoryFromQuery(dateFrom, dateTo, valueFilterType, valueFilterPlace, valueFilterSkill).then(model => {this.History = model; this.HistoryCount = model.length});
  }

  private SetDateToInput(){
      //TODO: Неужели в TS нет нормального Date.ToStringFormat ? Это ведь ужасно писать самому такой код
      var today = new Date();
      var d = today.getDate();
      var m = today.getMonth() + 1;
      var dd = "";
      var mm = "";

      var yyyy = today.getFullYear();
      if (d < 10) {
          dd = '0' + d;
      }
      else{
          dd = d.toString();
      }
      if (m < 10) {
          mm = '0' + m;
      }
      else{
          mm = m.toString();
      }
      let stringDate = yyyy + "-" + mm + "-" + dd;

     // (<HTMLInputElement>document.getElementById("dateTo")).value = stringDate;
  }

}
