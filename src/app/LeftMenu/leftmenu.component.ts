import { ILeftMenuDataProvider } from '../Services/i-left-menu-data-provider.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { LeftMenuModel } from '../Model/left-menu-model';

import { Injectable } from '@angular/core';

@Component({
  selector: 'leftmenu',
  templateUrl: './leftmenu.component.html',
  styleUrls: ['./leftmenu.component.css']
})

export class LeftMenuComponent implements OnInit {

  private leftMenuDataProvider: ILeftMenuDataProvider;
  public MenuModel: LeftMenuModel;
  public ShowMobileUpMenu: boolean = false;

  constructor(leftMenuDataProvider: ILeftMenuDataProvider) {

    this.MenuModel = LeftMenuModel.GetEmptyModel();
    this.leftMenuDataProvider = leftMenuDataProvider;
  }

  ngOnInit() {

    this.leftMenuDataProvider.GetLeftMenuNodel().then(model => {
      this.MenuModel = model;
    })
  }
}
