import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmountOfMaterialStudiedComponent } from './amount-of-material-studied.component';

describe('AmountOfMaterialStudiedComponent', () => {
  let component: AmountOfMaterialStudiedComponent;
  let fixture: ComponentFixture<AmountOfMaterialStudiedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmountOfMaterialStudiedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmountOfMaterialStudiedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
