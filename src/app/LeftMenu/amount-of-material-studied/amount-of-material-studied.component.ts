import { Component } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'amount-of-material-studied',
  templateUrl: './amount-of-material-studied.component.html',
  styleUrls: ['./amount-of-material-studied.component.css']
})

export class AmountOfMaterialStudiedComponent {

  @Input() BookCount: number;
  @Input() ArticleCount: number;
  @Input() VideoCount: number;

  @Input() AddBookCount: number;
  @Input() AddArticleCount: number;
  @Input() AddVideoCount: number;

  constructor() {

  }

}
