import { Component } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'avatar',
  templateUrl: 'avatar.component.html',
  styleUrls: ['avatar.component.css']
})

export class AvatarComponent {

  @Input() totalPoint: number;
  @Input() FullName: string;
  @Input() AddCoin: number;

  constructor() {

  }

}
