import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'site-section',
  templateUrl: 'site-section.component.html',
  styleUrls: ['site-section.component.css']
})

export class SiteSectionComponent {

  constructor(private router: Router){}
}
