import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExitSectionComponent } from './exit-section.component';

describe('ExitSectionComponent', () => {
  let component: ExitSectionComponent;
  let fixture: ComponentFixture<ExitSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExitSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExitSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
