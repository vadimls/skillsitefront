import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {SkilCardComponent } from './skilcard.component';

describe('SkilcardComponent', () => {
  let component: SkilCardComponent;
  let fixture: ComponentFixture<SkilCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkilCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkilCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
