import {SkilCardModel, SkilCardEx, SkilRowModel} from '../Model/skil-card-model';
import {Component, OnInit, OnDestroy, ApplicationRef, ChangeDetectorRef} from '@angular/core';
import { SkilcardDataProvider } from '../Services/skilcard-data-provider';
import {AbstractSkilRowDataProvider} from '../Services/AbstractSkilRowDataProvider';
import {ThreadSleeper} from '../Tools/threadSleeper';
import {DEBUG} from "@angular/compiler-cli/ngcc/src/logging/console_logger";

@Component({
  selector: 'skilcard',
  templateUrl: 'skilcard.component.html',
  styleUrls: ['skilcard.component.css'],
})

export class SkilCardComponent implements OnInit {

  public SkilCards: SkilCardModel[] = [];

  public SkilRows: SkilRowModel[] = [];

  public CurrentSkilCardModel: SkilCardModel;

  public ShowVideo: boolean = true;
  public ShowArticle: boolean = true;
  public ShowBook: boolean = true;
  public SortByRp: boolean = true;
  public SortBySp: boolean = false;
  public SortType: string = "По знанию";
  public SkillListIsOpen: boolean = false;

  private SkilcardProvider: SkilcardDataProvider;

  private SkilRowDataProvider: AbstractSkilRowDataProvider;



  constructor(skilcardDataProvider: SkilcardDataProvider, skilRowDataProvider: AbstractSkilRowDataProvider) {
    this.SkilcardProvider = skilcardDataProvider;
    this.SkilRowDataProvider = skilRowDataProvider;

  }

  ngOnInit() {
    this.SkilcardProvider.GetAllSkilCard().then(model => { this.SkilCards = model; this.SkilCardCortBySp() });
  }

  Count(c: number): number[] {
    let arr: number[] = [];
    for (var i = 0; i < c; i++)
      arr.push(i);

    return arr;
  }

  async SkilListCardContainerClick(skilCard: SkilCardModel){

      if (window.matchMedia("(max-aspect-ratio: 13/9)").matches){
          if (!this.SkillListIsOpen){
            this.SkilCards.forEach(card => {
               if (card.Id != skilCard.Id)
                   card.SkillCardVisable = false;
            });
            let currentCard = document.getElementById("skilcard" + skilCard.Id);
            let positionX = currentCard.getBoundingClientRect().left;
            let posotionTop = currentCard.getBoundingClientRect().top;
            currentCard.style.top =  "100VH";
            currentCard.style.position = "fixed";

              //TODO: Почему то без этой задержки не применяются промежуточные значения css-top а устанавливается последнее. Из за этой особеннсоти не срабатывает анимация. Нужно переделать, но пока не знаю как
              await ThreadSleeper.Delay(100);
              currentCard.classList.add("skil-list-card-container-root-fixed");
              currentCard.style.top = null;
              this.CurrentSkilCardModel = skilCard;
              this.SkillListIsOpen = true;
              await ThreadSleeper.Delay(1000);
              this.SkilCards.forEach(card => {
                  if (card.Id != skilCard.Id)
                      card.SkillCardForRemove = true;
              });

              this.SkilRowDataProvider.GetAllSkilRow(skilCard.Id).then(model => this.SkilRows = model);
          }
          else{
              this.SkillListIsOpen = false;
              this.SkilCards.forEach(card => {
                      card.SkillCardForRemove = false;
              });
              this.SkilCards.forEach(card => {
                      card.SkillCardVisable = true;
              });
              this.SkilRows = [];
              this.ShowVideo = true;
              this.ShowArticle = true;
              this.ShowBook  = true;
          }
      }
      else {
          if (this.SkillListIsOpen){
              this.SkillListIsOpen = false;
              this.SkilRows = [];
              this.ShowVideo = true;
              this.ShowArticle = true;
              this.ShowBook  = true;
          }else{
              this.SkillListIsOpen = true;
              this.CurrentSkilCardModel = skilCard;
              this.SkilRowDataProvider.GetAllSkilRow(skilCard.Id).then(model => this.SkilRows = model);
          }
      }
  }

  ClickOnFilterArticle(){
      this.ShowArticle = !this.ShowArticle;
  }

  ClickOnFilterVideo(){
      this.ShowVideo = !this.ShowVideo;
  }

  ClickOnFilterBook(){
      this.ShowBook = !this.ShowBook;
  }

  SkilCardCortByRp(){
      this.SortBySp = false;
      this.SortByRp = true;
      this.SkilCards = this.SkilCards.sort((a,b) => {
         return b.MoneyCount - a.MoneyCount
      })
  }

  SkilCardCortBySp(){
      this.SortBySp = true;
      this.SortByRp = false;
      this.SkilCards = this.SkilCards.sort((a,b) => {
          return (b.PracticalExperience + b.TheoreticalExperience ) - (a.PracticalExperience + a.TheoreticalExperience)
      })
  }

  ClickChangeSortType(){
        if(this.SortType === "По времени"){
            this.SkilCardCortByRp();
        }
        if(this.SortType === "По знанию"){
            this.SkilCardCortBySp();
        }
  }
}



