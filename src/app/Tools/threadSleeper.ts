export class ThreadSleeper {

    public static Delay(ms: number){
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}
