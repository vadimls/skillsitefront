import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpmenuComponent } from './upmenu.component';

describe('UpmenuComponent', () => {
  let component: UpmenuComponent;
  let fixture: ComponentFixture<UpmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
