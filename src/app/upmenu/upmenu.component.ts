import { Component, OnInit } from '@angular/core';
import {ILeftMenuDataProvider} from '../Services/i-left-menu-data-provider.service';
import {LeftMenuModel} from '../Model/left-menu-model';

@Component({
  selector: 'upmenu',
  templateUrl: './upmenu.component.html',
  styleUrls: ['./upmenu.component.css']
})
export class UpmenuComponent implements OnInit {

  private leftMenuDataProvider: ILeftMenuDataProvider;
  public MenuModel: LeftMenuModel;
  public ShowMobileUpMenu: boolean = false;

  constructor(leftMenuDataProvider: ILeftMenuDataProvider) {

    this.MenuModel = LeftMenuModel.GetEmptyModel();
    this.leftMenuDataProvider = leftMenuDataProvider;
  }

  ngOnInit() {

    this.leftMenuDataProvider.GetLeftMenuNodel().then(model => {
      this.MenuModel = model;
    })
  }

}
