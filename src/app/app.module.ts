import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {Routes, RouterModule} from '@angular/router';
import { registerLocaleData } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import localeRu from '@angular/common/locales/ru';

import { AppComponent } from './app.component';
import { LeftMenuComponent } from './LeftMenu/leftmenu.component';
import { SiteSectionComponent } from './LeftMenu/site-section/site-section.component';
import { ExitSectionComponent } from './LeftMenu/exit-section/exit-section.component';
import { AvatarComponent } from './LeftMenu/avatar/avatar.component';
import { AmountOfMaterialStudiedComponent } from './LeftMenu/amount-of-material-studied/amount-of-material-studied.component';
import { SkilCardComponent } from './skilcard/skilcard.component';
import { AmountOfMaterialStudiedConverterPipe } from './Pipes/amount-of-material-studied-converter.pipe';
import { SkilPointConverterPipe } from './Pipes/skil-point-converter.pipe';
import {ILeftMenuDataProvider} from './Services/i-left-menu-data-provider.service';
import {skipLast} from 'rxjs/operators';
import {SkilcardDataProvider} from './Services/skilcard-data-provider';
import {LeftMenuWebapiDataProviderService} from './Services/left-menu-webapi-data-provider.service';
import {SkilcardWebapiDataProviderService} from './Services/skilcard-webapi-data-provider.service';
import {LeftMenuTestdataProviderService} from './Services/left-menu-testdata-provider.service';
import {SkilcardTestdataProviderService} from './Services/skilcard-testdata-provider.service';
import { TestcssComponent } from './testcss/testcss.component';
import {AbstractSkilRowDataProvider} from './Services/AbstractSkilRowDataProvider';
import {SkilRowTestDataProviderService} from './Services/skil-row-test-data-provider.service';
import {SkilRowWebapiDataProviderService} from './Services/skil-row-webapi-data-provider.service';
import { HistoryComponent } from './History/history.component';
import {AbstractHistoryDataProvider} from './Services/AbstractHistoryDataProvider';
import {HistoryTestDataProviderService} from './Services/history-test-data-provider.service';
import {HistoryWebapiDataProviderService} from './Services/history-webapi-data-provider.service';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {StatisticsComponent } from './Statistics/statistics.component';
import {AbstractStatisticDataProvider} from './Services/abstract-statistic-data-provider';
import {StatisticTestDataProvider} from './Services/statistic-test-data-provider';
import {FormsModule} from '@angular/forms';
import {AbstractHistoryFilterProviderService} from './Services/abstract-history-filter-provider.service';
import {HistoryFilterTestdataProviderService} from './Services/history-filter-testdata-provider.service';
import {HistoryFilterWebApiProviderService} from './Services/history-filter-web-api-provider.service';
import { UpmenuComponent } from './upmenu/upmenu.component';
import { LightleftmenuComponent } from './lightleftmenu/lightleftmenu.component';

// определение маршрутов
const appRoutes: Routes =[
  { path: '', component: HistoryComponent},
  { path: 'skilList', component: SkilCardComponent},
  { path: 'history', component: HistoryComponent},
  { path: 'statistics', component: StatisticsComponent},
];

registerLocaleData(localeRu, 'ru');

@NgModule({
  declarations: [
    AppComponent,
    LeftMenuComponent,
    SiteSectionComponent,
    ExitSectionComponent,
    AvatarComponent,
    AmountOfMaterialStudiedComponent,
    SkilCardComponent,
    AmountOfMaterialStudiedConverterPipe,
    SkilPointConverterPipe,
    TestcssComponent,
    HistoryComponent,
    StatisticsComponent,
    UpmenuComponent,
    LightleftmenuComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, RouterModule.forRoot(appRoutes), FormsModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'ru' },
              { provide: LocationStrategy, useClass: HashLocationStrategy},
              { provide: ILeftMenuDataProvider, useClass: LeftMenuWebapiDataProviderService },
              { provide: SkilcardDataProvider, useClass: SkilcardWebapiDataProviderService},
              { provide: AbstractSkilRowDataProvider, useClass: SkilRowWebapiDataProviderService},
              { provide: AbstractHistoryDataProvider, useClass: HistoryWebapiDataProviderService},
              { provide: AbstractStatisticDataProvider, useClass: StatisticTestDataProvider},
              { provide: AbstractHistoryFilterProviderService, useClass: HistoryFilterWebApiProviderService}],

  bootstrap: [AppComponent]
})


export class AppModule { }
