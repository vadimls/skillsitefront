import { Component, ViewEncapsulation, NgModule } from '@angular/core';
import { hummerjs } from 'node_modules/hammerjs'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  hummerjs = hummerjs;
  title = 'AngularCliTest2';
}
