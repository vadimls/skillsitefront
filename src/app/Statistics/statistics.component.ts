  import { Component, OnInit, AfterViewInit, OnDestroy, AfterViewChecked } from '@angular/core';
  import * as c3 from 'c3'
  import {ChartAPI} from 'c3';
  import {StatisticsModel} from '../Model/statistics-model';
  import {StatisticDayModel} from '../Model/statistic-day-model';
  import {TotalPointModel} from '../Model/total-point-model';
  import {AbstractStatisticDataProvider} from '../Services/abstract-statistic-data-provider';
  import {StatisticCard} from '../Model/statistic-card';
  import {__await} from 'tslib';
  import {ThreadSleeper} from '../Tools/threadSleeper';
  import {TotalPointSkill} from '../Model/total-point-skill';
  import {SkilRowModel} from '../Model/skil-card-model';

  @Component({
    selector: 'app-statistics',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.css']
  })
  export class StatisticsComponent implements AfterViewInit, OnDestroy{

    private readonly PARAMETR_NAME_BOOK = 'книги';
    private readonly PARAMETR_NAME_ARTICLE = 'статьи';
    private readonly PARAMETR_NAME_VIDEO = 'видео';
    private readonly PARAMETR_NAME_TYPE_HOME = 'дома';
    private readonly PARAMETR_NAME_TYPE_WORK = 'на работе';
    private readonly PARAMETR_NAME_TYPE_WORK_ROAD = 'в дороге';
    private readonly PARAMETR_FIRST_COLOR = 'rgb(81,255,83)';
    private readonly PARAMETR_SECOND_COLOR = '#a7a4ff';
    private readonly PARAMETR_THIRD_COLOR = '#ffb520';

    private readonly HeaderChartNameOfSourceType: string[] = ['x', this.PARAMETR_NAME_BOOK, this.PARAMETR_NAME_ARTICLE, this.PARAMETR_NAME_VIDEO];
    private readonly HeaderChartNameOfGroupSourceType: string[] = [this.PARAMETR_NAME_BOOK, this.PARAMETR_NAME_ARTICLE, this.PARAMETR_NAME_VIDEO];
    private readonly ColorsForChartsByTypeSource = { 'книги': this.PARAMETR_FIRST_COLOR, 'статьи': this.PARAMETR_SECOND_COLOR, 'видео': this.PARAMETR_THIRD_COLOR};
    private readonly HeaderChartNameOfPlaceType: string[] = ['x', this.PARAMETR_NAME_TYPE_HOME, this.PARAMETR_NAME_TYPE_WORK, this.PARAMETR_NAME_TYPE_WORK_ROAD];
    private readonly HeaderChartNameOfGroupPlaceType: string[] = [this.PARAMETR_NAME_TYPE_HOME, this.PARAMETR_NAME_TYPE_WORK, this.PARAMETR_NAME_TYPE_WORK_ROAD];
    private readonly ColorsForChartsByPlaceType = { 'дома': this.PARAMETR_FIRST_COLOR, 'на работе': this.PARAMETR_SECOND_COLOR, 'в дороге': this.PARAMETR_THIRD_COLOR};
    private readonly HeaderChartNameOfTotalPoint: string[] = ['x', 'монет'];

    private DaysChart: ChartAPI;
    private TotalPointChart: ChartAPI;
    private TypePointChart: ChartAPI;

    private ResizeHandler: any = (event) => this.resizeCharts();

    private readonly StatisticDataProvider: AbstractStatisticDataProvider;

    public CurrentStatisticsModel: StatisticsModel;

    public CurrentStatisticCard: StatisticCard = StatisticCard.GetNewYearCard("загрузка", new Date().getFullYear());

    public StatisticCards: StatisticCard[];

    public StatisticCardCount = 0;

    public ShowCharts: boolean = false;

    public DataIsLoading = true;

    public Percent: number = 100;

    public TotalPointSkillModels: TotalPointSkill[] = [];

    private timeoutForScroll;

    public constructor(StatisticDataProvider: AbstractStatisticDataProvider) {
        this.StatisticDataProvider = StatisticDataProvider;
    }

      public async LoadStat(statisticCard: StatisticCard){
        this.scrollToTop();
        this.DataIsLoading = true;

        if (this.CurrentStatisticCard != null){
            this.CurrentStatisticCard.CardIsActive = false;
        }
        this.CurrentStatisticCard = statisticCard;
        this.CurrentStatisticCard.CardIsActive = true;

        if (!this.ShowCharts){
            this.ShowCharts = true;

            await ThreadSleeper.Delay(1000);
            this.StatisticDataProvider.GetStat(statisticCard).then(model => {
                this.CurrentStatisticsModel = model;
            });

            await this.WaitingLoadingCharts();
            this.DaysChart = this.getEmptyDaysChart();
         //   this.TotalPointChart = this.getEmptyTotalPointChart();
            this.TypePointChart = this.getEmptyTypePointChart();
            window.addEventListener("resize", this.ResizeHandler, false);
            await this.WaitingLoadModelForCharts();
            this.updateCharts();
            this.DataIsLoading = false;
        }
        else {

            await ThreadSleeper.Delay(1000);
            this.StatisticDataProvider.GetStat(statisticCard).then(model => {
                this.CurrentStatisticsModel = model;
                this.updateCharts();
            });
            this.DataIsLoading = false;
        }
    }

    ngAfterViewInit(): void {
        this.StatisticDataProvider.GetAllStatisticCards().then(model => {
            this.StatisticCards = model;
            this.StatisticCardCount = this.StatisticCards.length;
        });

    }

    private updateCharts(){

        //  chart.unload();
        //   chart.flush();
        let convertedDaysChart: any[] = this.CurrentStatisticsModel.ConvertStatisticByDayToArrayModel();
        convertedDaysChart.unshift(this.HeaderChartNameOfPlaceType);
        this.DaysChart.load({
            rows: convertedDaysChart
        });

        let convertedTotalPoint: any[] = this.CurrentStatisticsModel.ConvertStatisticByTotalPointModelToArrayModel();
        convertedTotalPoint.unshift(this.HeaderChartNameOfTotalPoint);
    //    this.TotalPointChart.load({
    //        rows: convertedTotalPoint
    //    });

        let totalMoneyCount: number = 0;
        this.CurrentStatisticsModel.StatisticByPoint.forEach(row => {
            totalMoneyCount += row.Value;
        });
        this.TotalPointSkillModels = [];
        this.CurrentStatisticsModel.StatisticByPoint.forEach(row => {
            this.TotalPointSkillModels.push(new TotalPointSkill(row.Header, row.Value, row.PathToIcon, (row.Value / totalMoneyCount) * 100))
        });

        let convertedTypePoint: any[] = this.CurrentStatisticsModel.StatisticByType;
        convertedTypePoint.unshift(this.HeaderChartNameOfGroupPlaceType);
        this.TypePointChart.load({
            rows: convertedTypePoint
        });
    }

    private getEmptyDaysChart(): ChartAPI {

      let elementHeight = this.getActualHeightParantDivChart();

      return c3.generate({
        bindto: '#daysChart',
          size: {
            height: elementHeight
          },
        data: {
          colors: this.ColorsForChartsByPlaceType,
          x: 'x',
          rows: [this.HeaderChartNameOfPlaceType],
          groups: [
            this.HeaderChartNameOfGroupPlaceType
          ],
          type: 'bar'
        },
          legend: {
            show: false,
              position: 'right'
          },
        axis: {
          x: {
            type: 'category' // this needed to load string x value
          }
        }
      });
    }

    private getEmptyTotalPointChart(): ChartAPI {

        let elementHeight = this.getActualHeightParantDivChart();

      return c3.generate({
        bindto: '#totalPointChart',
          size: {
              height: elementHeight
          },
        data: {
          colors: {
            монет: '#d3d3d3',
          },
          x: 'x',
          rows: [this.HeaderChartNameOfTotalPoint],
          type: 'bar'
        },
        axis: {
          rotated: true,
          x: {
            type: 'category'
          },
            y2:{
              label:{
                  text:"dfdf", position: 'inner-center'
              }
            }
        }
      });
    }

    private getEmptyTypePointChart(): ChartAPI {

        let elementHeight = this.getActualHeightParantDivChart();

        let g: any[] = [
            this.HeaderChartNameOfGroupPlaceType,
            [0, 0, 0]
        ];

        return  c3.generate({
            bindto: '#typePointChart',
            size: {
                height: elementHeight
            },

            data: {
                colors: this.ColorsForChartsByPlaceType,
                rows: g,
                type : 'donut',
            },
            legend: {
                show: true,
                position: 'bottom',
                inset : {anchor: "top-left", x:50}
            }
        });
    }

      private resizeCharts(): void {
          let newHeight = this.getActualHeightParantDivChart();
    //      this.TotalPointChart.resize( {height: newHeight});
          this.TypePointChart.resize( {height: newHeight});
          this.DaysChart.resize( {height: newHeight});
      }

      private getActualHeightParantDivChart(): number {
          return  document.getElementById("daysChart-div").clientHeight;
      }

      ngOnDestroy(): void {
          window.removeEventListener("resize", this.ResizeHandler, false);
      }

      private async WaitingLoadingCharts(){
        while (true){
            await ThreadSleeper.Delay(300);
            if (document.getElementById("daysChart-div") != null)
                return;
        }
      }

      private async WaitingLoadModelForCharts(){
          while (true){
              await ThreadSleeper.Delay(300);
              if (this.CurrentStatisticsModel != null)
                  return;
          }
      }

      private scrollToTop() {
          var top = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
          if(top > 0) {
              window.scrollBy(0,-100);
              this.timeoutForScroll = setTimeout(() => this.scrollToTop(),20);
          } else clearTimeout(this.timeoutForScroll);
          return false;
      }

  }
